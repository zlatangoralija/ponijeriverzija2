<?php

Use App\Contacts;
Use App\Accommodation;
Use App\Type;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController\FrontController@userPanel');

Route::get('/admin', 'AdminController\AdminController@adminPanel');

Route::get('/contacts', 'ContactController@index');
Route::get('/contact/add', 'ContactController@create');
Route::post('/contact/add', 'ContactController@store');
Route::get('/contact/edit/{id}', 'ContactController@edit');
Route::post('/contact/edit/{id}', 'ContactController@update');

Route::get('/types', 'TypeController@index');
Route::get('/type/add', 'TypeController@create');
Route::post('/type/add', 'TypeController@store');
Route::get('/type/edit/{id}', 'TypeController@edit');
Route::post('/type/edit/{id}', 'TypeController@update');

Route::get('/objects', 'ObjectController@index');
Route::get('/object/add', 'ObjectController@create');
Route::post('/object/add', 'ObjectController@store');
Route::get('/object/edit/{id}', 'ObjectController@edit');
Route::post('/object/edit/{id}', 'ObjectController@update');
Route::post('/object/edit/{id}', 'ObjectController@update');

Route::get('/object/delete/{id}', 'ObjectController@destroy');
Route::get('/contact/delete/{id}', 'ContactController@destroy');
Route::get('/type/delete/{id}', 'TypeController@destroy');

Route::get('/vikendica/{id}/{name}/', 'FrontController\FrontController@singleObject');

Route::post('/mail', 'FrontController\FrontController@sendMail');

Auth::routes();
Route::get('/admin', 'HomeController@index')->name('admin');
