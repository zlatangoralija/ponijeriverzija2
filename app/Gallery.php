<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'gallery';
    protected $fillable = ['img', 'objects_id' ];

    public function accommodation()
    {
        return $this->belongsTo('App\Accommodation', 'objects_id');
    }

}
