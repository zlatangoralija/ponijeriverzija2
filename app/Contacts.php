<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'contacts';
    protected $fillable = ['name', 'surname', 'location', 'contactPhone', 'contactViber', 'email', 'socialNetwork'];

    public function accommodations(){
        return $this->hasMany('App\Accommodation');

    }
    public function fullname(){
        return $this->name." ".$this->surname;

    }
}
