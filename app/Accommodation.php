<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accommodation extends Model
{
    protected $table = 'objects';
    protected $fillable = ['name', 'description', 'capacity', 'numberOfRooms', 'numberOfBeds', 'remarks', 'contacts_id', 'type_id', 'headerImage', 'wifi', 'parking', 'tv', 'pets', 'furnace', 'centralHeating', 'barbecue', 'email'];

    public function contacts()
    {
        return $this->belongsTo('App\Contacts');
    }

    public function types()
    {
        return $this->belongsTo('App\Type','type_id');
    }

    public function gallery()
    {
        return $this->hasMany('App\Gallery', 'objects_id');
    }
}
