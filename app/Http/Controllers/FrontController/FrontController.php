<?php

namespace App\Http\Controllers\FrontController;

use App\Contacts;
use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Accommodation;
Use App\Type;
use Illuminate\Support\Facades\Mail;

class FrontController extends Controller
{
    public function userPanel(){
        $objects = Accommodation::all();
        $types = Type::all();


//    if (!isset($_COOKIE['count']))
//    {
//        $cookie = 1;
//        setcookie("count", $cookie);
//    }
//    else
//    {
//        $cookie = ++$_COOKIE['count'];
//        setcookie("count", $cookie);
//    }


        return view("FrontViews.front", compact('objects', 'types'));
    }

    public function singleObject($id){
        $singleObject = Accommodation::find($id);
        $objects = Accommodation::all();
        $types = Type::all();
        $contacts = Contacts::find($id);
        $images = Accommodation::find($id)->gallery;

//        dd($images);
        return view("FrontViews.single_object", compact('objects', 'types', 'contacts', 'singleObject', 'images', 'newdata'));
    }

    /**
     *
     */
    public function sendMail(Request $request){
        $data = array(
            'email' => $request->email,
            'subject' => $request->subject,
            'mailMessage' => $request->message,
            'name' => $request->name
        );
        Mail::send('sendMail.email', $data, function ($message) use ($data){
            $message->from($data['email']);
            $message->to('podrskaponijeri@gmail.com');
            $message->subject($data['subject']);
        });

        return redirect("/");
    }

}
