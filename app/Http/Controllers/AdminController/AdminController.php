<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Accommodation;

class AdminController extends Controller
{
    public function adminPanel(){
        $objects = Accommodation::all();
        $objectsNumber = $objects->count();
        return view("AdminViews.admin", compact('objectsNumber'));
    }
}
