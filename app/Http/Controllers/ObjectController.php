<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Http\Controllers\AdminController\AdminController;
use Illuminate\Http\Request;
use App\Accommodation;
use App\Contacts;
use App\Type;
use Intervention\Image\ImageManagerStatic as Image;
use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\Input;
class ObjectController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objects = Accommodation::all();
        $objectsNumber = $objects->count();
        return view('AdminViews.objects', compact('objects', 'objectsNumber'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $objects = Accommodation::all();
        $objectsNumber = $objects->count();
        $contacts = Contacts::all();
        $types = Type::all();
        return view('AdminViews.object_add', compact('contacts', 'types', 'objectsNumber'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Accommodation::create($request->all());
        return redirect('/objects');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $objects = Accommodation::all();
        $objectsNumber = $objects->count();
        $contacts = Contacts::all();
        $types = Type::all();
        $object = Accommodation::find($id);
        return view('AdminViews.object_edit', compact('contacts', 'types', 'object', 'objectsNumber'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());

        $findObject = Accommodation::find($id);
        $findObject->update($request->all());

        Gallery::destroy('objects_id', $id);
        foreach ($request['img'] as $img) {
            $gallery = new Gallery();
            $gallery->objects_id=$id;
            $name = time() . $img->getClientOriginalName(); // prepend the time (integer) to the original file name
            $img->move('uploads', $name); // move it to the 'uploads' directory (public/uploads)
            $gallery->img=$name;
            $gallery->save();

            // // create instance of Intervention Image
            $img = Image::make('uploads/'.$name);
            $img->save(public_path().'/uploads/'.$name);

        }

        $headerImage = $request['headerImage'];
        $name = time() . $headerImage->getClientOriginalName(); // prepend the time (integer) to the original file name
        $headerImage->move('uploads', $name); // move it to the 'uploads' directory (public/uploads)
        $findObject->headerImage=$name;
        $findObject->save();

        // // create instance of Intervention Image
        $headerImage = Image::make('uploads/'.$name);
        $headerImage->save(public_path().'/uploads/'.$name);

        return redirect('/objects');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Accommodation::destroy($id);
        Gallery::where('objects_id', $id)->delete();
        return redirect('/objects');
    }
}
