<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Accommodation;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objects = Accommodation::all();
        $objectsNumber = $objects->count();
        return view('AdminViews.admin', compact('objectsNumber'));
    }
}
