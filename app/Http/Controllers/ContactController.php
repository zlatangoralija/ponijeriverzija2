<?php

namespace App\Http\Controllers;

use App\Contacts;
use App\Accommodation;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objects = Accommodation::all();
        $objectsNumber = $objects->count();
        $contacts = Contacts::all();
        return view ("AdminViews.contacts", compact('contacts', 'objectsNumber'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $objects = Accommodation::all();
        $objectsNumber = $objects->count();
        return view ('AdminViews.contact_add', compact('objectsNumber'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request->all());
        Contacts::create($request->all());
        return redirect('/contacts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $objects = Accommodation::all();
        $objectsNumber = $objects->count();
        $contacts = Contacts::find($id);
        return view('AdminViews.contact_edit', compact('contacts', 'objectsNumber'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $findContacts = Contacts::find($id);
        $findContacts->update($request->all());
        return redirect('/contacts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Contacts::destroy($id);
        return redirect()->back();
    }
}
