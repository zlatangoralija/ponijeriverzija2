<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableObjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objects', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name', 50);
            $table->longtext('description');
            $table->integer('capacity');
            $table->integer('numberOfRooms');
            $table->integer('numberOfBeds');
            $table->longtext('remarks');
            $table->integer('contacts_id');
            $table->integer('type_id');
            $table->char('headerImage');
            $table->char('wifi')->nullable();
            $table->char('parking')->nullable();;
            $table->char('tv')->nullable();;
            $table->char('pets')->nullable();;
            $table->char('furnace')->nullable();;
            $table->char('centralHeating')->nullable();;
            $table->char('barbecue')->nullable();;
            $table->char('email')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Objects');
    }
}
