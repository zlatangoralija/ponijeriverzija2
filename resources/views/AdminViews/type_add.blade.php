@extends('master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Ponijeri Kakanj
                <small>Admin panel</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Main content -->
        @include('partials.box')
        <!-- Main content -->

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">New type</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="/type/add" method="POST">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="box-body">
                        <div class="form-group col-xs-6">
                            <label>Type name</label>
                            <input type="text" class="form-control" name="type" placeholder="Enter type name">
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Type description</label>
                            <textarea class="form-control" rows="7" name="description" placeholder="Enter type description"></textarea>
                        </div>
                        <div class="form-group col-xs-6">
                            <label>Type remarks</label>
                            <input type="text" class="form-control" name="remarks" placeholder="Enter type remarks">
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
    </div>
    </div>
    <!-- ./wrapper -->

    @endsection

