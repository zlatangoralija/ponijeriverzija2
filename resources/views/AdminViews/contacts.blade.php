@extends('master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Ponijeri Kakanj
                <small>Admin panel</small>
            </h1>
        </section>

        <section class="content">

        <!-- Main content -->
        @include('partials.box')
         <!-- Main content -->

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Pregled svih kontakata</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>ID</th>
                            <th>Ime</th>
                            <th>Prezime</th>
                            <th>Mjesto</th>
                            <th>Kontakt telefon</th>
                            <th>Kontakt Viber</th>
                            <th>E-mail</th>
                            <th>Socijalne mreže</th>
                            <th>Akcija</th>

                        </tr>
                        @foreach($contacts as $contact)
                            <tr>
                                <td>{{$contact->id}}</td>
                                <td>{{$contact->name}}</td>
                                <td>{{$contact->surname}}</td>
                                <td>{{$contact->location}}</td>
                                <td>{{$contact->contactPhone}}</td>
                                <td>{{$contact->contactViber}}</td>
                                <td>{{$contact->email}}</td>
                                <td>{{$contact->socialNetwork}}</td>
                                <td><div class="btn-group">
                                        <a type="button" href="/contact/edit/{{$contact->id}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                        <button type="button" onclick="deleteObject({{$contact->id}})" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                    </div></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>

    <!-- ./wrapper -->
@endsection
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    function deleteObject(id){
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
            if (willDelete) {
                location.href='/contact/delete/'+id;
            }
        })
    }

</script>