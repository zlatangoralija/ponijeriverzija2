@extends('master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Ponijeri Kakanj
                <small>Admin panel</small>
            </h1>
        </section>

        <section class="content">
            <!-- Main content -->
            @include('partials.box')
            <!-- Main content -->

                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Forma za doavanje novog kontakta</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form role="form" action="/contact/add" method="POST">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="box-body">
                                    <div class="form-group col-xs-3">
                                        <label>Ime kontakta</label>
                                        <input type="text" class="form-control" name="name" placeholder="Unesite ime kontakta">
                                    </div>
                                    <div class="form-group col-xs-3">
                                        <label>Prezime kontakta</label>
                                        <input type="text" class="form-control" name="surname" placeholder="Unesite prezime kontakta">
                                    </div>
                                    <div class="form-group col-xs-3">
                                        <label>Mjesto stanovanja kontakta</label>
                                        <input type="text" class="form-control" name="location" placeholder="Unesite mjesto stanovanja kontakta">
                                    </div>
                                    <div class="form-group col-xs-3">
                                        <label>Telefonski broj kontakta</label>
                                        <input type="text" class="form-control" name="contactPhone" placeholder="Unesite telefonski broj kontakta">
                                    </div>
                                    <div class="form-group col-xs-3">
                                        <label>Viber broj kontakta</label>
                                        <input type="text" class="form-control" name="contactViber" placeholder="Unesite Viber broj kontakta">
                                    </div>
                                    <div class="form-group col-xs-3">
                                        <label>E-mail adresa kontakta</label>
                                        <input type="text" class="form-control" name="email" placeholder="Unesite E-mail adresu kontakta">
                                    </div>
                                    <div class="form-group col-xs-3">
                                        <label>Socijalne mreže kontakta</label>
                                        <input type="text" class="form-control" name="socialNetwork" placeholder="Unesite socijalne mreze kontakta">
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Spremi</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.box -->
                <!-- /.row -->
            </section>
        </section>
    </div>

            <!-- Control Sidebar -->

            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
    <!-- ./wrapper -->
  @endsection

