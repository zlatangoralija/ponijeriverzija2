@extends('master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Ponijeri Kakanj
                <small>Admin panel</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
        <!-- Main content -->
        @include('partials.box')
        <!-- Main content -->

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">List of all objects</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Pretraga">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>ID</th>
                            <th>Object name</th>
                            <th>Capacity</th>
                            <th>Number of rooms</th>
                            <th>Number of beds</th>
                            <th>Remarks</th>
                            <th>Kontakt</th>
                            <th>Tip</th>
                            <th>Action</th>

                        </tr>
                        @foreach($objects as $object)
                        <tr>
                            <td>{{$object->id}}</td>
                            <td>{{$object->name}}</td>
                            <td>{{$object->capacity}}</td>
                            <td>{{$object->numberOfRooms}}</td>
                            <td>{{$object->numberOfBeds}}</td>
                            <td>{{$object->remarks}}</td>
                            <td>{{$object->contacts->fullname()}}</td>
                            <td>{{$object->types->type}}</td>
                            <td><div class="btn-group">
                                    <a type="button" href="/object/edit/{{$object->id}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                    <button type="button" onclick="deleteObject({{$object->id}})" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                </div></td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
    </div>
    @endsection
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    function deleteObject(id){
        swal({
            title: "Da li ste sigurni?",
            text: "Da li ste sigurni da želite obrisati ovaj fajl?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
            if (willDelete) {
               location.href='/object/delete/'+id;
            }
    })
    }

</script>