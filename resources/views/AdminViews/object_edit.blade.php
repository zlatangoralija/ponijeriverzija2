@extends('master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Ponijeri Kakanj
                <small>Admin panel</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Main content -->
        @include('partials.box')
        <!-- Main content -->

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit object</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="/object/edit/{{$object->id}}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Object name</label>
                                <input type="text" class="form-control" name="name" value="{{$object->name}}" placeholder="Enter object name">
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" rows="7" name="description" placeholder="Enter object description">{{$object->description}}</textarea>
                            </div>
                            <div class="form-group col-xs-4">
                                <label>Capacity</label>
                                <input type="text" class="form-control" name="capacity" value="{{$object->capacity}} "placeholder="Enter capacity">
                            </div>
                            <div class="form-group col-xs-4">
                                <label>Number of rooms</label>
                                <input type="text" class="form-control" name="numberOfRooms" value="{{$object->numberOfRooms}}" placeholder="Enter number of rooms">
                            </div>
                            <div class="form-group col-xs-4">
                                <label>Number of beds</label>
                                <input type="text" class="form-control" name="numberOfBeds" value="{{$object->numberOfBeds}}" placeholder="Enter number of beds">
                            </div>
                            <div class="form-group col-xs-12">
                                <label>Options</label>
                                    <table id="example2" class="table table-bordered table-hover">
                                        <tr>
                                            <thead>
                                            <th>Wi-fi</th>
                                            <th>Parking</th>
                                            <th>TV</th>
                                            <th>Pets</th>
                                            <th>Furnace</th>
                                            <th>Central heating</th>
                                            <th>Barbecue</th>
                                            <th>E-mail</th>
                                            </thead>
                                        </tr>
                                        <tbody>
                                        <tr>
                                            @if($object->wifi == 1)
                                            <td><input type="radio" name="wifi" value="1" checked="checked"><label>Yes</label>
                                                <input type="radio" name="wifi" value="0"><label>No</label></td>
                                                @else
                                                <td><input type="radio" name="wifi" value="1"><label>Yes</label>
                                                <input type="radio" name="wifi" value="0" checked="checked"><label>No</label></td>
                                            @endif
                                            @if($object->parking == 1)
                                                <td><input type="radio" name="parking" value="1" checked="checked"><label>Yes</label>
                                                    <input type="radio" name="parking" value="0"><label>No</label></td>
                                                @else
                                                    <td><input type="radio" name="parking" value="1"><label>Yes</label>
                                                        <input type="radio" name="parking" value="0" checked="checked"><label>No</label></td>
                                            @endif
                                            @if($object->tv == 1)
                                                <td><input type="radio" name="tv" value="1" checked="checked"><label>Yes</label>
                                                    <input type="radio" name="tv" value="0"><label>No</label></td>
                                                @else
                                                    <td><input type="radio" name="tv" value="1"><label>Yes</label>
                                                        <input type="radio" name="tv" value="0" checked="checked"><label>No</label></td>
                                            @endif
                                            @if($object->pets == 1)
                                                <td><input type="radio" name="pets" value="1" checked="checked"><label>Yes</label>
                                                    <input type="radio" name="pets" value="0"><label>No</label></td>
                                                @else
                                                    <td><input type="radio" name="pets" value="1"><label>Yes</label>
                                                        <input type="radio" name="pets" value="0" checked="checked"><label>No</label></td>
                                            @endif
                                            @if($object->furnace == 1)
                                                <td><input type="radio" name="furnace" value="1" checked="checked"><label>Yes</label>
                                                    <input type="radio" name="furnace" value="0"><label>No</label></td>
                                                @else
                                                    <td><input type="radio" name="furnace" value="1"><label>Yes</label>
                                                        <input type="radio" name="furnace" value="0" checked="checked"><label>No</label></td>
                                            @endif
                                            @if($object->centralHeating == 1)
                                                <td><input type="radio" name="centralHeating" value="1" checked="checked"><label>Yes</label>
                                                    <input type="radio" name="centralHeating" value="0"><label>No</label></td>
                                                @else
                                                    <td><input type="radio" name="centralHeating" value="1"><label>Yes</label>
                                                        <input type="radio" name="centralHeating" value="0" checked="checked"><label>No</label></td>
                                            @endif
                                            @if($object->barbecue == 1)
                                                <td><input type="radio" name="barbecue" value="1" checked="checked"><label>Yes</label>
                                                    <input type="radio" name="barbecue" value="0"><label>No</label></td>
                                                @else
                                                    <td><input type="radio" name="barbecue" value="1"><label>Yes</label>
                                                        <input type="radio" name="barbecue" value="0" checked="checked"><label>No</label></td>
                                            @endif
                                            @if($object->email == 1)
                                                <td><input type="radio" name="email" value="1" checked="checked"><label>Yes</label>
                                                    <input type="radio" name="email" value="0"><label>No</label></td>
                                                @else
                                                    <td><input type="radio" name="email" value="1"><label>Yes</label>
                                                        <input type="radio" name="email" value="0" checked="checked"><label>No</label></td>
                                            @endif
                                        </tr>
                                        </tbody>
                                    </table>
                            </div>
                            <div class="form-group col-xs-6">
                                <label>Select contact</label>
                                <div class="form-group">
                                    <select class="form-control" name="contacts_id">
                                        <option value="{{$object->contacts->id}}">{{$object->contacts->fullname()}}</option>
                                        @foreach($contacts as $contact)
                                            <option value="{{$contact->id}}">{{$contact->name}} {{$contact->surname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-xs-6">
                                <label>Select type</label>
                                <div class="form-group">
                                    <select class="form-control" name="type_id">
                                        <option value="{{$object->types->id}}">{{$object->types->type}}</option>
                                    @foreach($types as $type)
                                            <option value="{{$type->id}}">{{$type->type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-xs-6">
                                <label>Remarks</label>
                                <textarea class="form-control" rows="7" name="remarks" placeholder="Enter remarks...">{{$object->remarks}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Index image:</label>
                                <input type="file" name="headerImage" value="{{$object->headerImage}}">
                                {{--<img src="{{asset('FrontAssets/img/smjestaj/')}}/{{$object->headerImage}}" style="width: 100px;">--}}

                                <p class="help-block">Image that will be displayed on index page.</p>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">Image gallery</label>
                                <input type="file" name="img[]" multiple>

                                <p class="help-block">Choose x images for that will be displayed on gallery page.</p>
                            </div>
                            <!-- /.box-body -->
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>

                    </form>
                </div>
        </section>
        </div>
@endsection

{{--@section('extra_footer')--}}
{{--<script>--}}
    {{--$( document ).ready(function() {--}}
        {{--var myDropzone = new Dropzone("div#dz", { url: "/file/post"});--}}
    {{--});--}}


{{--</script>--}}
    {{--@endsection--}}
