<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Izletište Ponijeri-Kakanj</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="FrontAssets/img/favicon.png" rel="icon">
    <link href="FrontAssets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="FrontAssets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="FrontAssets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="FrontAssets/lib/animate/animate.min.css" rel="stylesheet">
    <link href="FrontAssets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="FrontAssets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="FrontAssets/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="FrontAssets/css/style.css" rel="stylesheet">

</head>

<header id="header">
    <div class="container-fluid">

        <div id="logo" class="pull-left">
            <h1><a href="#intro" class="scrollto">Ponijeri Kakanj</a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>-->
        </div>

        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="menu-active"><a href="#intro">Početna</a></li>
                <li><a href="#about">O nama</a></li>
                <li><a href="#portfolio">Smještaj</a></li>
                <li><a href="#contact">Kontakt</a></li>
            </ul>
        </nav><!-- #nav-menu-container -->
    </div>
</header><!-- #header -->

<section id="intro">
    <div class="intro-container">
        <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

            <ol class="carousel-indicators"></ol>

            <div class="carousel-inner" role="listbox">

                <div class="carousel-item active">
                    <div class="carousel-background"><img src="FrontAssets/img/intro-carousel/ponijeri1.jpeg" alt=""></div>
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2>Dobrodošli</h2>
                            <h2><font size="5">Izletište Ponijeri-Kakanj. Idealno odmaralište za porodicu i prijatelje! </font></h2>
                            <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>
                        </div>
                    </div>
                </div>

                <div class="carousel-item">
                    <div class="carousel-background"><img src="FrontAssets/img/intro-carousel/ponijeri2.jpg" alt=""></div>
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2>Dobrodošli</h2>
                            <h2><font size="5">Izletište Ponijeri-Kakanj. Idealno odmaralište za porodicu i prijatelje!</font></h2>
                            <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>
                        </div>
                    </div>
                </div>

                <div class="carousel-item">
                    <div class="carousel-background"><img src="FrontAssets/img/intro-carousel/ponijeri3.jpg" alt=""></div>
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2>Dobrodošli</h2>
                            <h2><font size="5">Izletište Ponijeri-Kakanj. Idealno odmaralište za porodicu i prijatelje!</font></h2>
                            <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>
                        </div>
                    </div>
                </div>

                <div class="carousel-item">
                    <div class="carousel-background"><img src="FrontAssets/img/intro-carousel/4.jpg" alt=""></div>
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2>Dobrodošli</h2>
                            <h2><font size="5">Izletište Ponijeri-Kakanj. Idealno odmaralište za porodicu i prijatelje!</font></h2>
                            <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>
                        </div>
                    </div>
                </div>

                <div class="carousel-item">
                    <div class="carousel-background"><img src="FrontAssets/img/intro-carousel/5.jpg" alt=""></div>
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2>Dobrodošli</h2>
                            <h2><font size="5">Izletište Ponijeri-Kakanj. Idealno odmaralište za porodicu i prijatelje!</font></h2>
                            <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>
                        </div>
                    </div>
                </div>

            </div>

            <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>
    </div>
</section><!-- #intro -->

<main id="main">

    <section id="about">
        <div class="container">

            <header class="section-header">
                <h3>O Ponijerima</h3>
                <p><font size="4">Nadomak Kaknja, 19 km udaljeno od gradskog središta, na nadmorskoj visini od 1144-1200 n/m, nalazi se prelijepo izletište Ponijeri, koje je i zračna banja i planinsko izletište,  zimsko odmaralište ali i lovačko polazište. Ponijeri su popularno izletište sa uređenim stazama za šetnju, odmor i rekreaciju, a zbog bogate flore postoji mogućnost branja ljekovitog bilja, šumskog voća, gljiva. Ovo izletište je i popularno lovište jer se u okolnim šumama mogu naći zečevi, lisice i jazavci a dublje u šumi mogu se uloviti srna i medvjed. U zimskom periodu Ponijeri su idealno sankalište i skijalište sa uređenim ski stazama.</font></p>
            </header>

            <div class="row about-cols">

                <div class="col-md-4 wow fadeInUp">
                    <div class="about-col">
                        <div class="img">
                            <img src="FrontAssets/img/coffee.jpg" alt="" class="img-fluid">
                            <div class="icon"><i class="ion-coffee"></i></div>
                        </div>
                        <h2 class="title">Idealno mjesto za odmor</h2>
                        <p>Ponijeri su prelijepo vikend naselje za izlete i uživanje. Udaljeno je oko 20 kilometara od Kaknja. Nadmorska visina Ponijera je negdje oko 1.000 metara, a kote okolo Ponijera su više od 1.000 metara.</p>
                    </div>
                </div>

                <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="about-col">
                        <div class="img">
                            <img src="FrontAssets/img/skijanje.jpg" alt="" class="img-fluid">
                            <div class="icon"><i class="ion-ios-snowy"></i></div>
                        </div>
                        <h2 class="title">Prvo narodno skijalište u BiH</h2>
                        <p>
                            Termini i cijene:<br>
                            Dnevna karta: od 09:00h do 15:45h <strong>15 KM</strong><br>
                            Poludnevna karta: od 09:00h do 12:30h - <strong>10 KM</strong><br>
                            Poludnevna karta: od 12:30h do 15:45h - <strong>10 KM</strong><br>
                            Noćna karta: od 18:00h od 21:00h - <strong>10KM</strong><br>
                        </p>
                    </div>
                </div>

                <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="about-col">
                        <div class="img">
                            <img src="FrontAssets/img/smjestaj.jpg" alt="" class="img-fluid">
                            <div class="icon"><i class="ion-home"></i></div>
                        </div>
                        <h2 class="title">Veliki izbor smještaja</h2>
                        <p>Na Ponijerima možete pronaći veliki broj smještaja koji su dostupni za iznajmljivanje, i to, po jako niskim cijenama. Na našem portalu možete da pogledate neke od smještaja i stupite u kontakt sa vlasnicima.</p>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- #about -->

    <section id="call-to-action" class="wow fadeIn">
        <div class="container text-center">
            <h3>SKIJALIŠTE: ZATVORENO</h3>
            <p> Skijalište je trenutno zatvoreno usljed kraja sezone.</p>
        </div>
    </section><!-- #call-to-action -->


    <section id="portfolio"  class="section-bg" >
        <div class="container">

            <header class="section-header">
                <h3 class="section-title">Smještaj na ponijerima</h3>
                <p> Na našem portalu možete pronaći sve dostupne smještaje za iznajmljivanje po jako niskim cijenama </p>
            </header>

            <div class="row">
                <div class="col-lg-12">
                    <ul id="portfolio-flters">
                        <li data-filter="*" class="filter-active">SVE</li>
                        @foreach($types as $type)
                        <li data-filter=".filter-{{$type->type}}">{{$type->type}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="row portfolio-container">

                @foreach($objects as $object)

                <div class="col-lg-4 col-md-6 portfolio-item filter-{{$object->types->type}} wow fadeInUp">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="{{asset('uploads')}}/{{$object->headerImage}}" class="img-fluid" alt="">
                            <center><a href="/vikendica/{{$object->id}}/{{$object->name}}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a></center>
                        </figure>
                        <div class="portfolio-info">
                            <h4><a href=/vikendica/{{$object->id}}/{{$object->name}}">{{$object->name}}</a></h4>
                            <p>Kapacitet: {{$object->capacity}}</p>
                            @if($object->wifi) <img src="FrontAssets/img/opcije/wifi.jpg" class="img-fluid" title="Besplatna Wi-Fi konekcija">  @endif
                            @if($object->parking) <img src="FrontAssets/img/opcije/parking.jpg" class="img-fluid" title="Obezbjeđen parking">  @endif
                            @if($object->tv) <img src="FrontAssets/img/opcije/tv.jpg" class="img-fluid" title="Televizor">  @endif
                            @if($object->pets) <img src="FrontAssets/img/opcije/pets.jpg" class="img-fluid" title="Dozvoljeni ljubimci">  @endif
                            @if($object->furnace) <img src="FrontAssets/img/opcije/furnace.jpg" class="img-fluid" title="Peć na drva">  @endif
                            @if($object->centralHeating) <img src="FrontAssets/img/opcije/centralHeating.jpg" class="img-fluid" title="Centralno grijanje">  @endif
                            @if($object->barbecue) <img src="FrontAssets/img/opcije/barbecue.jpg" class="img-fluid" title="Vanjski roštilj">  @endif
                            @if($object->email) <img src="FrontAssets/img/opcije/email.jpg" class="img-fluid" title="Kontakt E-mail">  @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
    </section><!-- #portfolio -->

    <section id="clients" class="wow fadeInUp">
        <div class="container">

            <header class="section-header">
                <h3>Prijatelji portala</h3>
            </header>

            <div class="owl-carousel clients-carousel">
                <img src="FrontAssets/img/clients/client-1.png" alt="">
                <img src="FrontAssets/img/clients/client-2.png" alt="">
                <img src="FrontAssets/img/clients/client-3.png" alt="">
                <img src="FrontAssets/img/clients/client-4.png" alt="">
                <img src="FrontAssets/img/clients/client-5.png" alt="">
                <img src="FrontAssets/img/clients/client-6.png" alt="">
                <img src="FrontAssets/img/clients/client-7.png" alt="">
                <img src="FrontAssets/img/clients/client-8.png" alt="">
            </div>

        </div>
    </section><!-- #clients -->

    <section id="testimonials" class="section-bg wow fadeInUp">
        <div class="container">

            <header class="section-header">
                <h3>Komentari</h3>
            </header>

            <div class="owl-carousel testimonials-carousel">

                <div class="testimonial-item">
                    <img src="FrontAssets/img/testimonial-1.jpg" class="testimonial-img" alt="">
                    <h3>Saul Goodman</h3>
                    <h4>Ceo &amp; Founder</h4>
                    <p>
                        <img src="FrontAssets/img/quote-sign-left.png" class="quote-sign-left" alt="">
                        Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                        <img src="FrontAssets/img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="FrontAssets/img/testimonial-2.jpg" class="testimonial-img" alt="">
                    <h3>Sara Wilsson</h3>
                    <h4>Designer</h4>
                    <p>
                        <img src="FrontAssets/img/quote-sign-left.png" class="quote-sign-left" alt="">
                        Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                        <img src="FrontAssets/img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="FrontAssets/img/testimonial-3.jpg" class="testimonial-img" alt="">
                    <h3>Jena Karlis</h3>
                    <h4>Store Owner</h4>
                    <p>
                        <img src="FrontAssets/img/quote-sign-left.png" class="quote-sign-left" alt="">
                        Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                        <img src="FrontAssets/img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="FrontAssets/img/testimonial-4.jpg" class="testimonial-img" alt="">
                    <h3>Matt Brandon</h3>
                    <h4>Freelancer</h4>
                    <p>
                        <img src="FrontAssets/img/quote-sign-left.png" class="quote-sign-left" alt="">
                        Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                        <img src="FrontAssets/img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="FrontAssets/img/testimonial-5.jpg" class="testimonial-img" alt="">
                    <h3>John Larson</h3>
                    <h4>Entrepreneur</h4>
                    <p>
                        <img src="FrontAssets/img/quote-sign-left.png" class="quote-sign-left" alt="">
                        Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                        <img src="FrontAssets/img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>

            </div>

        </div>
    </section><!-- #testimonials -->

    <section id="contact" class="section-bg wow fadeInUp">
        <div class="container">

            <div class="section-header">
                <h3>Kontak</h3>
                <p>Kontaktirajte osoblje zaduženo za vođenje i uređivanje stranice</p>
            </div>

            <div class="row contact-info">

                <div class="col-md-4">
                    <div class="contact-address">
                        <i class="ion-ios-location-outline"></i>
                        <h3>Adresa</h3>
                        <address><a href="https://goo.gl/maps/oXRcp9kyBT72" target="_blank">Izletište Ponijeri, 72240 Kakanj, BiH</a></address>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="contact-phone">
                        <i class="ion-ios-telephone-outline"></i>
                        <h3>Telefon</h3>
                        <p><a href="tel:+38762274120">+387 62 274-120</a> </p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="contact-email">
                        <i class="ion-ios-email-outline"></i>
                        <h3>E-mail</h3>
                        <p><a href="mailto:info@example.com">ponijerikakanj@gmail.com</a></p>
                    </div>
                </div>

            </div>

            <div class="form">
                <div id="sendmessage">Poruka uspješno poslana!</div>
                <div id="errormessage"></div>
                <form action="/mail" method="post" role="form">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Vaše ime" data-rule="minlen:4" data-msg="Molimo unesite najmanje 4 karaktera" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Vaš e-mail" data-rule="email" data-msg="Molimo unesite validnu e-mail adresu" />
                            <div class="validation"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Tema" data-rule="minlen:4" data-msg="Molimo unesite najmanje 8 karaktera" />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Molimo napišite nešto" placeholder="Poruka"></textarea>
                        <div class="validation"></div>
                    </div>
                    <div class="text-center"><button type="submit" class="btn btn-primary">Pošalji</button></div>
                </form>
            </div>

        </div>
    </section><!-- #contact -->

    <footer id="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-info">
                        <h3>Ponijeri Kakanj</h3>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Navigacija</h4>
                        <ul>
                            <li><i class="ion-ios-arrow-right"></i> <a href="#">Početna</a></li>
                            <li><i class="ion-ios-arrow-right"></i> <a href="#">O nama</a></li>
                            <li><i class="ion-ios-arrow-right"></i> <a href="#">Reklame</a></li>
                            <li><i class="ion-ios-arrow-right"></i> <a href="#">Smještaj</a></li>
                            <li><i class="ion-ios-arrow-right"></i> <a href="#">Kontakt</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-contact">
                        <h4>Kontaktirajte nas</h4>
                        <p>
                            Izletište Ponijeri<br>
                            72240 Kakanj, BiH <br>
                            <strong>Telefon</strong> +387 62 274-120<br>
                            <strong>E-mail:</strong> ponijerikakanj@gmail.com<br>
                        </p>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-newsletter">
                        <h4>Socijalne mreže</h4>
                        <div class="social-links">
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                            <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                        </div>
                        <br>
                        <br>
                        <h4>Važni brojevi</h4>
                        <p>
                            <strong>Policija:</strong> 111<br>
                            <strong>Vatrogasci:</strong> 111<br>
                            <strong>Hitna pomoć:</strong> 111<br>
                            <strong>GSS:</strong> 111<br>
                        </p>
                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong>Ponijeri Kakanj - 2018</strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!--
                  All the links in the footer should remain intact.
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=BizPage
                -->
                <p hidden>Best <a href="https://bootstrapmade.com/">Bootstrap Templates</a> by BootstrapMade</p>
            </div>
        </div>
    </footer><!-- #footer -->

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

</main>

<!-- JavaScript Libraries -->
<script src="FrontAssets/lib/jquery/jquery.min.js"></script>
<script src="FrontAssets/lib/jquery/jquery-migrate.min.js"></script>
<script src="FrontAssets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="FrontAssets/lib/easing/easing.min.js"></script>
<script src="FrontAssets/lib/superfish/hoverIntent.js"></script>
<script src="FrontAssets/lib/superfish/superfish.min.js"></script>
<script src="FrontAssets/lib/wow/wow.min.js"></script>
<script src="FrontAssets/lib/waypoints/waypoints.min.js"></script>
<script src="FrontAssets/lib/counterup/counterup.min.js"></script>
<script src="FrontAssets/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="FrontAssets/lib/isotope/isotope.pkgd.min.js"></script>
<script src="FrontAssets/lib/lightbox/js/lightbox.min.js"></script>
<script src="FrontAssets/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
<!-- Contact Form JavaScript File -->
<script src="FrontAssets/contactform/contactform.js"></script>

<!-- Template Main Javascript File -->
<script src="FrontAssets/js/main.js"></script>

</body>
</html>