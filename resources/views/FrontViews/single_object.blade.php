<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{$singleObject->name}}</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="{{asset('FrontAssets/img/favicon.png')}}" rel="icon">
    <link href="{{asset('FrontAssets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="{{asset('FrontAssets/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="{{asset('FrontAssets/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('FrontAssets/lib/animate/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('FrontAssets/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('FrontAssets/lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('FrontAssets/lib/lightbox/css/lightbox.min.css')}}" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="{{asset('FrontAssets/css/style.css')}}" rel="stylesheet">
</head>

<header id="header">
    <div class="container-fluid">

        <div id="logo" class="pull-left">
            <h1><a href="#intro" class="scrollto">{{$singleObject->name}}</a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>-->
        </div>

        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="menu-active"><a href="/">Početna</a></li>
                <li><a href="#about">{{$singleObject->name}}</a></li>
                <li><a href="#portfolio">Foto galerija</a></li>
                <li><a href="/#portfolio">Smještaj</a></li>
                <li><a href="#contact">Kontakt</a></li>
            </ul>
        </nav><!-- #nav-menu-container -->
    </div>
</header><!-- #header -->

<section id="intro">
    <div class="intro-container">
        <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

            <div class="carousel-inner" role="listbox">

                <div class="carousel-item active">
                    <div class="carousel-background"><img src="{{asset('uploads')}}/{{$singleObject->headerImage}}" alt=""></div>
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2>{{$singleObject->name}}</h2>
                            <h2><font size="5">{{$singleObject->remarks}}</font></h2>
                            <a href="#featured-services" class="btn-get-started scrollto">Get Started</a>
                        </div>
                    </div>
                </div>

                </div>

            </div>
        </div>
    </div>
</section><!-- #intro -->

<main id="main">

    <section id="about">
        <div class="container">

            <header class="section-header">
                <h3>{{$singleObject->name}}</h3>
                <p><font size="4">{{$singleObject->description}}</font></p>
            </header>

            <div class="row about-cols">

                <div class="col-md-12 wow fadeInUp">
                    <div class="about-col">
                        <h2 class="title">Informacije o kontaktu</h2>
                        <center>
                            <table class="table">
                                <tr>
                                    <th>Ime i prezime</th>
                                    <th>Lokacija</th>
                                    <th>Broj telefona</th>
                                    <th>Viber</th>
                                    <th>E-mail</th>
                                </tr>
                                <tr>
                                    <td>{{$singleObject->contacts->fullname()}}</td>
                                    <td>{{$singleObject->contacts->location}}</td>
                                    <td>{{$singleObject->contacts->contactPhone}}</td>
                                    <td>{{$singleObject->contacts->contactViber}}</td>
                                    <td>{{$singleObject->contacts->email}}</td>
                                </tr>
                            </table>
                        </center>
                    </div>
                </div>

                <div class="col-md-12 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="about-col">
                        <h2 class="title">Informacije o smještaju</h2>
                        <center>
                            <table class="table">
                                <tr>
                                    <th>Tip objekta</th>
                                    <th>Kapacitet</th>
                                    <th>Broj soba</th>
                                    <th>Broj kreveta</th>
                                    <th>Napomene</th>
                                </tr>
                                <tr>
                                    <td>{{$singleObject->types->type}}</td>
                                    <td>{{$singleObject->capacity}}</td>
                                    <td>{{$singleObject->numberOfRooms}}</td>
                                    <td>{{$singleObject->numberOfBeds}}</td>
                                    <td>{{$singleObject->remarks}}</td>
                                </tr>
                            </table>
                        </center>
                    </div>
                </div>

                <div class="col-md-12 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="about-col">
                        <h2 class="title">Dodatne informacije o smještaju</h2>
                        <center>
                            @if($singleObject->wifi) <img src="{{asset('FrontAssets/img/opcije/wifi.jpg')}}" class="img-fluid opcije" title="Besplatna Wi-Fi konekcija">  @endif
                            @if($singleObject->parking) <img src="{{asset('FrontAssets/img/opcije/parking.jpg')}}" class="img-fluid opcije" title="Obezbjeđen parking">  @endif
                            @if($singleObject->tv) <img src="{{asset('FrontAssets/img/opcije/tv.jpg')}}" class="img-fluid opcije" title="Televizor">  @endif
                            @if($singleObject->pets) <img src="{{asset('FrontAssets/img/opcije/pets.jpg')}}" class="img-fluid opcije" title="Dozvoljeni ljubimci">  @endif
                            @if($singleObject->furnace) <img src="{{asset('FrontAssets/img/opcije/furnace.jpg')}}" class="img-fluid opcije" title="Peć na drva">  @endif
                            @if($singleObject->centralHeating) <img src="{{asset('FrontAssets/img/opcije/centralHeating.jpg')}}" class="img-fluid opcije" title="Centralno grijanje">  @endif
                            @if($singleObject->barbecue) <img src="{{asset('FrontAssets/img/opcije/barbecue.jpg')}}" class="img-fluid opcije" title="Vanjski roštilj">  @endif
                            @if($singleObject->email) <img src="{{asset('FrontAssets/img/opcije/email.jpg')}}" class="img-fluid opcije" title="Kontakt E-mail">  @endif
                        </center>
                        <br>
                    </div>
                </div>
            </div>

        </div>
    </section><!-- #about -->

    <section id="portfolio"  class="section-bg" >
        <div class="container">
            <header class="section-header">
                <h3 class="section-title">FOTO GALERIJA</h3>
            </header>
            <div class="row portfolio-container">
                @foreach($images as $img)
                <div class="col-lg-4 col-md-6 portfolio-item wow fadeInUp">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="{{asset('uploads')}}/{{$img->img}}" class="img-fluid" alt="">
                            <a href="{{asset('uploads')}}/{{$img->img}}" data-lightbox="portfolio" data-title="{{$singleObject->name}}" class="link-preview" title="Pogledaj"><i class="ion ion-eye"></i></a>
                        </figure>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

    </section><!-- #portfolio -->

    <section id="clients" class="wow fadeInUp">
        <div class="container">

            <header class="section-header">
                <h3>Prijatelji portala</h3>
            </header>

            <div class="owl-carousel clients-carousel">
                <img src="{{asset('FrontAssets/img/clients/client-1.png')}}" alt="">
                <img src="{{asset('FrontAssets/img/clients/client-2.png')}}" alt="">
                <img src="{{asset('FrontAssets/img/clients/client-3.png')}}" alt="">
                <img src="{{asset('FrontAssets/img/clients/client-4.png')}}" alt="">
                <img src="{{asset('FrontAssets/img/clients/client-5.png')}}" alt="">
                <img src="{{asset('FrontAssets/img/clients/client-6.png')}}" alt="">
                <img src="{{asset('FrontAssets/img/clients/client-7.png')}}" alt="">
                <img src="{{asset('FrontAssets/img/clients/client-8.png')}}" alt="">
            </div>

        </div>
    </section><!-- #clients -->

    <section id="contact" class="section-bg wow fadeInUp">
        <div class="container">

            <div class="section-header">
                <h3>Kontak</h3>
                <p>Kontaktirajte osoblje zaduženo za vođenje i uređivanje stranice</p>
            </div>

            <div class="row contact-info">

                <div class="col-md-4">
                    <div class="contact-address">
                        <i class="ion-ios-location-outline"></i>
                        <h3>Adresa</h3>
                        <address><a href="https://goo.gl/maps/oXRcp9kyBT72" target="_blank">Izletište Ponijeri, 72240 Kakanj, BiH</a></address>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="contact-phone">
                        <i class="ion-ios-telephone-outline"></i>
                        <h3>Telefon</h3>
                        <p><a href="tel:+38762274120">+387 62 274-120</a> </p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="contact-email">
                        <i class="ion-ios-email-outline"></i>
                        <h3>E-mail</h3>
                        <p><a href="mailto:info@example.com">ponijerikakanj@gmail.com</a></p>
                    </div>
                </div>

            </div>

            <div class="form">
                <div id="sendmessage">Poruka uspješno poslana!</div>
                <div id="errormessage"></div>
                <form action="/mail" method="post" role="form">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Vaše ime" data-rule="minlen:4" data-msg="Molimo unesite najmanje 4 karaktera" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Vaš e-mail" data-rule="email" data-msg="Molimo unesite validnu e-mail adresu" />
                            <div class="validation"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Tema" data-rule="minlen:4" data-msg="Molimo unesite najmanje 8 karaktera" />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Molimo napišite nešto" placeholder="Poruka"></textarea>
                        <div class="validation"></div>
                    </div>
                    <div class="text-center"><button type="submit">Pošalji</button></div>
                </form>
            </div>

        </div>
    </section><!-- #contact -->

    <footer id="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-info">
                        <h3>Ponijeri Kakanj</h3>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Navigacija</h4>
                        <ul>
                            <li><i class="ion-ios-arrow-right"></i> <a href="/">Početna</a></li>
                            <li><i class="ion-ios-arrow-right"></i> <a href="/#about">O nama</a></li>
                            <li><i class="ion-ios-arrow-right"></i> <a href="/#portfolio">Smještaj</a></li>
                            <li><i class="ion-ios-arrow-right"></i> <a href="/#contact">Kontakt</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-contact">
                        <h4>Kontaktirajte nas</h4>
                        <p>
                            Izletište Ponijeri<br>
                            72240 Kakanj, BiH <br>
                            <strong>Telefon</strong> +387 62 274-120<br>
                            <strong>E-mail:</strong> ponijerikakanj@gmail.com<br>
                        </p>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-newsletter">
                        <h4>Socijalne mreže</h4>
                        <div class="social-links">
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                            <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                        </div>
                        <br>
                        <br>
                        <h4>Važni brojevi</h4>
                        <p>
                            <strong>Policija:</strong> 111<br>
                            <strong>Vatrogasci:</strong> 111<br>
                            <strong>Hitna pomoć:</strong> 111<br>
                            <strong>GSS:</strong> 111<br>
                        </p>
                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong>Ponijeri Kakanj - 2018</strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!--
                  All the links in the footer should remain intact.
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=BizPage
                -->
                <p hidden>Best <a href="https://bootstrapmade.com/">Bootstrap Templates</a> by BootstrapMade</p>
            </div>
        </div>
    </footer><!-- #footer -->

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

</main>


<!-- JavaScript Libraries -->
<script src="{{asset('FrontAssets/lib/jquery/jquery.min.js')}}"></script>
<script src="{{asset('FrontAssets/lib/jquery/jquery-migrate.min.js')}}"></script>
<script src="{{asset('FrontAssets/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('FrontAssets/lib/easing/easing.min.js')}}"></script>
<script src="{{asset('FrontAssets/lib/superfish/hoverIntent.js')}}"></script>
<script src="{{asset('FrontAssets/lib/superfish/superfish.min.js')}}"></script>
<script src="{{asset('FrontAssets/lib/wow/wow.min.js')}}"></script>
<script src="{{asset('FrontAssets/lib/waypoints/waypoints.min.js')}}"></script>
<script src="{{asset('FrontAssets/lib/counterup/counterup.min.js')}}"></script>
<script src="{{asset('FrontAssets/lib/owlcarousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('FrontAssets/lib/isotope/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('FrontAssets/lib/lightbox/js/lightbox.min.js')}}"></script>
<script src="{{asset('FrontAssets/lib/touchSwipe/jquery.touchSwipe.min.js')}}"></script>
<!-- Contact Form JavaScript File -->
<script src="{{asset('FrontAssets/contactform/contactform.js')}}"></script>

<!-- Template Main Javascript File -->
<script src="{{asset('FrontAssets/js/main.js')}}"></script>

</body>
</html>